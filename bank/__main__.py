from funksion import exchange, print_course, question, change_balances, cleaning_bd


def main():
    cleaning_bd()
    change_balances()
    print('\nCOURSE USD or COURSE UAH\nEXCHANGE USD or EXCHANGE UAH\nSTOP\n')
    while True:
        c = question()
        match f'{c[0]}':
            case'STOP':
                print('SERVICE STOPPED')
                break
            case 'COURSE':
                print_course(c)
            case 'EXCHANGE':
                exchange(c)


if __name__ == '__main__':
    main()
