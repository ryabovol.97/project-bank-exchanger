from db import db
from datetime import datetime
from bank.db import delete_db
import json
import requests
URL = 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5'


ost = {
    'USD': 1000.0,
    'UAH': 4000.0
}


def load_exchange():
    c = json.loads(requests.get(URL).text)
    return c[0]


def cleaning_bd():
    while True:
        delete = input('Удалить все содержимое в базе данных? да/нет: ')
        match delete:
            case'да':
                delete_db()
                break
            case'нет':
                print('База данных не изменилась')
                break
            case _:
                print('Не верный ввод, попробуйте еще раз.')
                continue


def change_balances():
    while True:
        print(f'\nОстатки на сейчас: USD = {ost["USD"]}, UAH = {ost["UAH"]}')
        change = input('Изменить остатки? да/нет: ')
        if change == 'да':
            ost["USD"] = float(input('Сколько USD? '))
            ost["UAH"] = float(input('Сколько UAH? '))
            break
        elif change == 'нет':
            print(f'Остатки не изменились: USD = {ost["USD"]}, UAH = {ost["UAH"]}')
            break
        else:
            print('Не верный ввод, попробуйте еще раз.')
            continue


def question():
    print('\nCOMMAND?')
    return input('>>> ').split(" ")


def print_course(x):
    if x[1] == 'USD':
        print(f'RATE: {"%.2f" % float(load_exchange()["sale"])}, AVAILABLE: {"%.2f" % ost["USD"]}')
    elif x[1] == 'UAH':
        print(f'RATE: {"%.2f" % float(load_exchange()["buy"])}, AVAILABLE: {"%.2f" % ost["UAH"]}')
    else:
        print(f'INVALID CURRENCY {x[1]}')


def exchange(x):
    if x[1] == 'USD':
        uah = float(x[2])
        if (uah * float(load_exchange()["buy"])) > float(ost["UAH"]):
            print(f'UNAVAILABLE, REQUIRED BALANCE UAH {uah * float(load_exchange()["buy"])} '
                  f'AVAILABLE {"%.2f" % (ost["UAH"])}')
        else:
            amount = uah * float(load_exchange()["buy"])
            ost["UAH"] = float(ost["UAH"]) - (uah * float(load_exchange()["buy"]))
            ost["USD"] = float(ost["USD"]) + uah
            db('USD', str("%.2f" % uah), str(load_exchange()["buy"]),  str("%.2f" % amount),  str("%.2f" % ost["USD"]),
               str("%.2f" % ost["UAH"]), str(datetime.now().strftime("%H:%M:%S / %d-%m-%Y")))
            print(f'UAH {"%.2f" %amount}, RATE {"%.2f" % float(load_exchange()["buy"])}')
    elif x[1] == 'UAH':
        usd = float(x[2])
        if (usd / float(load_exchange()["sale"])) > float(ost["USD"]):
            print(f'UNAVAILABLE, REQUIRED BALANCE UAH {usd / float(load_exchange()["sale"])} '
                  f'AVAILABLE {"%.2f" % (ost["USD"])}')
        else:
            price_uah = 1 / float(load_exchange()["sale"])
            amount = usd * price_uah
            ost["USD"] = float(ost["USD"]) - (usd / float(load_exchange()["sale"]))
            ost["UAH"] = float(ost["UAH"]) + usd
            db('UAH', str("%.2f" % usd), str(load_exchange()["sale"]), str("%.2f" % amount), str("%.2f" % ost["USD"]),
               str("%.2f" % ost["UAH"]), str(datetime.now().strftime("%H:%M:%S / %d-%m-%Y")))
            print(f'USD {"%.4f" % amount}, RATE {"%.6f" % price_uah}')
    else:
        print(f'INVALID CURRENCY "{x[1]}"')
