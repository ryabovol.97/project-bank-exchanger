import sqlite3

base = sqlite3.connect('data.db')
cur = base.cursor()


def create_db():
    base.execute('CREATE TABLE IF NOT EXISTS {}(Currency, Amount, Exchange_rate, '
                 'Exchange_result, USD_ost, UAH_ost, Data)'.format('data'))
    base.commit()


def db(currency, amount, exchange_rate, exchange_result, usd, uah, data):
    create_db()
    cur.execute('INSERT INTO data VALUES(?, ?, ?, ?, ?, ?, ?)', (currency, amount, exchange_rate,
                                                                 exchange_result, usd, uah, data))
    base.commit()


def delete_db():
    base.execute('DELETE FROM data;',)
    base.commit()
